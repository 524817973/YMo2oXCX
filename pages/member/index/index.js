var e = getApp(),
  r = e.requirejs("core"),
  t = e.requirejs("wxParse/wxParse");
Page({
  data: {
    route: "member",
    icons: e.requirejs("icons"),
    member: {},
    isShow: false,
    inviteQR: '',
    avatar: false,
    isHide: '',
    isinformation: 0,
    memberBG: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXcAAADmBAMAAADco9ifAAAAHlBMVEX7WaP7fJr7hpf7kZX7cZ37m5L7ZqD6po/6soz6wIieL/7SAAAkSElEQVR42qycvU4jWRCFN+rcll/A8AIrIx4ACTleybJIHVgTb4JMuNnMY2/99ld1G4ExFG1Peubo3Lqnftp//VRMf50mjdOvRZx/7ff75/P+eYjNk8Rm9bTqsV1L3G3vWuzu73a73cPuocTfPwX8dJIvhT6dNEC+lwj08kdsNoI90ANfca+22/V2e7cu8O/vBb3Fgzw/CX7yR0JAy3fl/GzEC/Czowe5fW0CeaAX3EK8ABfolfR7+VLe7+V5kMfi8fEHwJ/kOckj4ZyjnLPCF+zOe4+np2fnvRG/MuLvRDaN+p3xbtiT+MefYz6o/zX9MuTTLBp5zhpGfuI20p+d+A3EbwW7aUYjcYtgHLpRD/qfYX5S6CeTusmmCR7ilflKvwte4G9WhCpeCFfk+gXzJvkHRZ/Qf4x5wW7Eh+BP81lV5QR0/a7EZ6oBvbLumllDvaE29pV5BP8zzE/GvJEuoJcHdq+h0FuY4EPz6F1Pq0AX/HBu0pFQ2PrPHIeHf37guCb6TDUTmgG90N6Il2/lfFSNPIa+ntWieaXeiT8cfoL505TJhlQD83Fcn/VpzHso9oS/Feh+VMGOcox8JH+Q57vgJ+c+D6tQvzisKXkE0xXPBbuNu1WZD+W42AO6MA/2H2BecatowhiYaNANabJK/slyvN9PaEYPazJPFN4VetqDgwvnn5/Ik867OwODDm4uqCB/49eTxQre42o1veuHHI+pqYfVqP8OeM+QSTyejPspBU+adNbjeirYLc+sHDs5PjNN6gbVPEp8WzacVqUe3s+CfW/YQW7QjfyieIhfk+MDN4oX2EB/FPSimm/K5qTY59tJmT9V5l00jn1f0rv7ApXN7Mm2oZlVTTT3/kWevN/N2DW+KxtjPg+rYSdFIhp9iE2gF/zOPboRxdiDYpx898FYMuH9u7KZ+v1EmsRQSnTgEN+LEGNdIhNlEp++QOA37BY3Mx/AUbyEkY/ijfnBGmxM8hotw5upsfppPKp3yruiL4fVJX9ENrcxj+L9qyg+VCMZEjcpPvgpQ7Hn3RquzKDLA/zMNYV4w64B898WzXhUUzPK/DORNn6lny6ataNXzZPjswaB+McZ/fE7zJ/isAbpfsHihkk0exTvaX7lNxSyUfhRdzfma/20I0/C/O33U7YMROkgd8Uj+T2sG/PcUFhJu6HUz5BpdqSaQJ+KF+JV8Ifj8XsHlvgVxFO4WuU32PhqyAjL8RaOHhuf8BGNezJFfzvzk3Luj5Fu2QZbk47SDM35jGiew5EVI7+O0xqK7znezmr4MUyNaeZ4s+aRjF9QWEnHHrUfyIe6FeZNM2mFS67ZyeNh2Cn+IgQ74G+t/tJKTkBXyVB178f7iVYNkpewu3Urnxm6PJ7juV5F8C55RX+bbJRyg0+SL2W3As+7tXoyazPRqimOzMKaNXO4E6bbkdBD8N9l3lt86cYmbLwyb9HdpNJO0d1v11UyXxxZzfEZpPjDEc1/OcGb5pH8NLZVAzvOxg+rl639hlIruVLJbEGvAfZdIDdf4MdV0QP+y8bAgsuV+snRZ1sY6Cge6Cp2o93S5LYST47fFeIRDXn+Bks292qsZUD1FIo37KAP2oP5DPrCrbOKMQjgEK+h8OUP8F/tGGSiXNiaID6w7+kYOHplHmOwdsX71ZrndWd+7M6NMJp/TMX77Xq8DTxNYQvyDKmGYcKiRYYxMOzyEdSOHi8p0Llbi2bCxx8F/m3gJ3qTRrp8o3hL8mSahO894bTxY9mthWu3k5lqPMnvIF4C4m8AH9jjtMqnBYc1Mg3tjg3EM7/JJE/HYJfYTTaFeLCr5AH/1WZ89cKme4CnZLqLp2GgkqdDZqo30QT0ofSzVBPEZ/2kuOVzm2ymIF+CVgcnlsqV02rmINETWy7X3h9jAIWpET/mcYNsIJ4Ub45saoox7MvhWQQNA3pkmibdGhDhC1LvmBoEf5tsvKlNV7j3hZ12xY5usMKKHdoVedrJNGRecFuQbLw5mTleybd4+SJ4gLubmVr9RBTsde731G2Bsx5VN3mSVk3A9xxp2F3tt8pGsLud9BsW0RfFg33sTq70g5E3xQv0ETsTVyItmTzxP3j9GvgpdFMUM3XFx8wVxWuCL8cV1cwtgwZ9pw+zswJ79sIRL8fjv1++XE8peHJ8Uzy9jkBvzNNkQvHKvieasfBO+LuBdz+tgv8G5k/BfNStUXRPEI/ehflzuAJ5IjLHkybv7GHwl4rfdeIjjsZ/Ev96I/MMXFG78+7ctxpk7u61mes6IkRDkkxL1nkPM+zIb2DetC7fDp1ptxOvzGea5IJ15rOCah2yuXLdNuIlujMw1I+kmpn416+kSu9Mspni2KehN2llN8xHX9VJ109nXrHTL7Cq+/1Mk6YmVPPyVeZP/hh2v15F9Lj4Wj/hyDBk1Qrn1Urtp5HEe46Xf/BjIfoDzGtcDz5wp25IkugmTQ3zJ+Md5pmeyScED/ay3FGZp2VwbJr/Sqo8eaIPuZ/SFzDAyWYHtsDRpyEj0YTcTTZFNXOqqdAPAd188CO8w/wX1plwB+/6+Mb8xrJkG/0xpffOKrOzerU+lPkZmjlwWjWuP7BT/OGElX+ApxWumnH0Gn32tFbVZ4LfVmMwV0/Y+IN+SDWHm5hXsQd0nHBgp7/3HPAZnSH5nmlM8ggezXA/BfPUIHlgX+3r9fVKzU9pyUDf8atgzg6/3U+abVaMcPygquznFbjtrHhEc7/jcnoIF4xqXhX4qzAP+M/hs5YyOknNlenjwU5T2JyBPhgD79TYF1crxzWjCh7e9Y66nnl3ks69F96QHveTndbqhTe04+VpqWZsx9+no/Rudm2spo9vOV6wX615Ad3LkF81z+9bm6mEA4/bqU5xaAqvxx7ZnfDemc/iD+xfZ36SQPHVkJlqSPKMEZR2g15tfKxiuWiw8faVloajGn7MuXfdvBrzl5erNT+Z5DEGwTojKGeeWcJTMu/k9zZTSL53OzKYuNYsD+8B/3rmT64YGjV9uQNfQJb0PTJ6NZga+YqlT/kk8dkjU80ztMROYmsEc0C/XAPeBD97YXl67HFkGIOwBhJg703h3usoU8sW9Peq5l8i/v2S5BnU97t12e3w2s/cZM/xKD6IN84Z4egXgsnTmlZeST9ajhfiL/9c52liekaSISgAa92aDe15CkJ/LwuoccmATg19poUVDuyGHuY/7hhg49msqbhN7EX0NAwEOmbYEzweflwyELnTFcYJcz+92OfV4nIN80o6bWGop4TyqKMEWjWOm07NyqCv9ROioc+knzpxTUdGtknsQvsFzX+k9tIxmJglUH04bqIzX4fd6+wz1dZqswW7LppsFwj6kLxerhrK/HWpMojHGHBDySeMwRni+z4QzKfeB+zphVn7xBiYm+RuDcEL75fP87zBhnl5sPHRkH/OQQhBqtl0W+ATVwsuV/cEu8jxwC/1E+gdu+rmGuYFegS+ILDbl2qn9SY3UXRnYxX4+U4C1+u9+0nWJkFeVHNA7hqpecB/cD8Z/VxRE6rRsGVb0KuJT/ybaoRd8AvmVTXR1x4mrn5aEzqZ0kRj6D8Bb8hxkydOq6v9HNEnrr7MxNCSNG9pBsnvhHkGrm0XS5F7kGfckQntgl3jQ/BTcn/yjkGkdxR/Zs85c3ysxjPDGZzwclxstBvzpPiDM59HFfyhnMsVmjfYJJolds809XZK5oP7Tck0vEREvwPm+8g1/RjORlFnmrx8zvxk8FlZHXf7Gbnue4pfGjKl3UTTVlO88jPFR7ZB7jRWE7plmsT++rlspjytmmi64NsIB+LlDyvM7YQz2BbJhKmB97F0tUwJfE83F2eeS+oDU8N6/MIXOHJN9C1PMgRpnkxlMwwSPBQ7c0uH/ZB6eRxrP880nx/YfrFO4I8FOCzNufp4C4o/HNmdN4ZH4ukxoXmXuzyJXYFzP30qm5OXUGDvK3Bnjyr4TXmDqEFf6xM3E+d1F4JXuWPjg/dkngRPopEvmP9A8tVOwrtJpmA/1w5f+rEIUvzKqG/b8bSZappnge+wtPEmeMB/OCzGB5/G4o8VCRKNaj5SjZ1XOgZ0yPqO8y5u11r4cVjlMTNpSTKP6+U65kk1LNY47fJhSo8PfqbqJniVRfCDnlWskmaCeNpMlH+WJf35GLyBJtUsX7XM2VnTvDy8rUjh6rNuLldup0QPfCPeAlfzqh9VDMCvsAe4yd7gY4bTlsiYWZa7dYuf9LIbR4bkgf7wnhUOyRh68L/9+9EL3c3HE0APbzAuxyt8YhthKb7OcFLx+jBLmLHnvFhwu27mPHnFgZ1fO6Oj7dBzcpmS2WezQ+G7apqpiUl3UQ0rcG7jd2XnM8Pw986qu/gZ/u/3wZ9qP/495p/prNY0acH1RJdJsVc/5sznHITTio0/GO1BvGgmFK/Q/fN2ufx3nZ8EviUaZZ4kyWF9Zq//aVzu6FNLpiCKnTw5l92keIPuBzawf8w8HQNobylemcfEO3rHvlicjFQDcEuRQbyhZ/7U9pwZdZfDKnX3jB3mF0VIbB/yliv107zYX+6n4uK7alaF9zWyiQC7BmmSTON3FIYM7DA/HlW2nAM5aZJB/eJ+Yt2WrU/LkwGdopsVvkRO0Y3gIX5Mk4KeVEkwf0I3oJ8TTfwhed75q12ywA7rCp99W/vM2Glpq62hGU8RMseb/AGeUNaN/siQ5bSWPtl4Wj14V5G79c6zPFW3Mh/rEV01y+UOs2SUT4n/TZ/faL5PXH1MH8xDfLXxbWCMaOoNReHqpqxtSJhqCGY4425K8K6nFeLf3pXNCSu83HJOVyOwy/yJNTJDjqvR8OPaf7mDw4qPlz/eSpBP5kicQbmeLh8wT5OMVKPYx20mIpjftNIvszz7tj3Lu2TA7sSj9+rjLU+SaiQWms/9bF/voN1RRBPQcZNtRv9UV4XLbwGw9mmRazUt12ALkIwe2Lm9p0ae+A3zBN3JnCJAPNsdbJins4H5oXLlvTkKV37HoPbjXfEYgyNZkhpEBCO0vyGb99dScsED4uuW87kSn2OQvtm/jR4ZEZ3JOU1Gj+yRDXP543ayj2KPPAPt+h94V/MsSFicRuKfl5L3KHdruIKc00P8DkdWAtE8ond5MsDu1Cvxf/5dIg8/KcQH9RCv2MPDj4pfvnnm+MPE0zEIyWR/MvUea6umGc7qkdOa6AW4oZdo4AWz7x9StirvSH5u1OzBrsGbZ4Yd5kM1zOmN+eXINTQT2GGeUUL3NSp7DizByNWTJBMc3vhzP4lg+PELVuCyN9mXmUzxnuMpQUjx7uQfe5/JfTzAnXmyTV9YpXKtxoB2fMkyKIfBGVdrpJvW1M6Km7fp2/WKI4vyyeE3/Mb7nz/twE5THle6THMw6ibVSMC7oidJuuID75q7qcxweB8E5KMXxtUU4uO8puaBT4MsHRlWuN2umMlxcxLmw8uPryuSamA+oloaDdAn9lTOnzeYZy9l0Z3ECtNnyjkOrmbVFB8T15Q7ism6uyzwGfNuJ8f5k0GXqGdVwrC/vbnmQd+rboj3IqS9lTD+jgGq4W5dsV/u1JMnW6bJxuqB06q0hzFA8EjHAvCTP6WAqh0yfyWBMcjYqpGn362rbGfX9h7rti542ky99nvhvJo1qLAz5MT+O7wYPbZWOa4MoDitVvyFbMY3dL1Hxn4864cmGpjPF9JJ8iwgkuCp/i6e5P8o8xBPwwDdsKSNq6EAqSVI+zky5vTd0yzfSqjvpLepZWCPwR/1k7J+Ueh/0LwfVccfZvKEatIKd1dj6LmeNmNbOH3wsGLOa90kSjNkkM70zOQOfCFdof+25z+Qe3vyV2geyfBSQqYa0mTrGEC8Ue+1H8Qn9J1Jnp7wPMMh2dAwqB0PAb5gHsnzawDMupX09gJUX49g7oer0eeO6g/mJYBv2FmRaP09/eAMSliaFM71uArzmMkU/Ix+4ePdDdNmMuwBH+LnTo1E/DP+ckdfgMthce/vGe+uGYww+Ub9wX+9RaYxrgSd7ctjMPFpanCT+fN7qxj9EWBH7oEfZwDzKRvHT66RcGMjD5q3s5o1d881ZBr7wtNIMCuGeVYPq5usOZ7DGja+5XiKv4rc9O6eTHmfmT9Zs4bKtbU7eKViZJ49Z4dPotHzylYNOwbYSRd7muEcnbEHR0e78m6qUfSCXcGz2SEfY31cxIos31+nXzD/FMhV8uOPrNaOdqtBNOgKO3Cl/kJ/7wXoityJN9XMzPNLasr82BXO+gnWOa2jkX/3t2rYI2MO8jh2VsfjOvSFf2cF5dCd+Sl2+BaiYb0czQM9RwmL5Q46fCVH3sV6gTHPHOT4f2PnchzXFQPRnfdWOQHZGdARuMqlBFwqJ8AcyAzo2Xk5zNb49T0A3og0ZjjatrrwcPHpi6duRx+dfa9mR5/hvCnAy+UNfTAfLkNOk0kNzOs6CNiZWpp5pOnijqM9bMMExMJg58ZfqsvRRygzINDQWzWrIJ/Me4jH5feaHS1ZTebTc8jH9s4RDUESOUkBWbzFGpxGyIfPUPu1Kcgh3SwiTYCnQUblqgIQuSqsI2cqU6RBOCnRKj0D6qeRk3XVp8LkzCdXkM+zKdC/ivmgPngX80TJc772+RmqT6bF4TIBn3nruHiWDi/i6U6S0jh6Wh3xEfHZIkvuHTjMK48/un4cnlYNvfh2uRjlZGA/iXx/WlsN8nhnSgQbDiikKVhE+DDHjtuo3xHQe2bwjF44TtY5gnKf50I6NbdWSFBAsUvNPiyQmC0yNDWS2zb0b5ytARzmlcQH82QGq8F3yWn+ZO5H3cpiXvzdUGu11KMFEnP8VP5OIgzzgb78BvCj3TGCpAcbAy7q2x4DXVdE3eHY87ZiYsfjHb7ae6CXjqzlkqTCUbbK5wN05vDl7xluYP4niMeU1uAyMH/0TH+0MQj7yPD4KUxBHVHVH0eUIvwlm+Q/8ArxnflyedB/C5/Rw0oFZahZ2T9rbtrxgm/+oud1r5xUXgDyjJI6XLFwliTeCJfLi3kNn/LTjL7qsLYTeYd46lahD/hRsxLlhT2Yx1R1G+fXaBMuk6mw7O7gFWv2zknyeA4pJn+Xa6IinqzmSf14tYW5XCzhZPyRBzv8NbPEZZx5+0L9v20dWSknEfBx549UmI2Tq1PjLs9S4b0HLtBflwSNe91CPqsQjRLS4Rv0e7iNGecTxpKgtQlurw9k/95a2c/OF2105o7rUqY47iA+c3iGZ4letZ//NLdhH9kq/dAe7luuSg120V3tAiZQCjRCj8NPmTPEG+2SNIn4wJ7Ud5+5Nea5/TSJ34ogITfI+DyhBuBd2T/mrZJpM6dfqppVgLwoRuanM/+vg09dCsi1CcBNGgOwD8nqXgYwdM5SprjHGPttwZEjJ8Aj7ghzt2+NGv9J3hUn8fl4WgP/WKWN1yzmKV3V8AiHD7d5ECUROhNqvEvmwFNwK3EHPbLr8UThKuS38PkMkzsVltfg8ax7YXJG7RfoaRisW91Pj8Ik51MFyTqgcByNb1R363QV/mReUxDQH7tegFLlusKkcEM8aYGgs2SVPJ5LOHJ5qM9+9kuFGqcd4u/OvDJh8JPIs2WHC1BpQk4q/GVKJx14kx6SCjv2wzz6CPspf+8eE+T3IA98Z15e0wcJrHvpqhpu/DEI6a8b4IiKfAzmx9YRmSGHelUgDn22O4x2oHuoxONvdsLK4zFyAxL5LeDDHL76NGtezPzpN5yGZWr0mYr201oFenP4HzCPFosFfKpBtq5fg5DLAr6mSyna6ax2XQpLsYZGG208PbJkXj2y6fHJ/Nmk1ntNNFab22xx/Pb4jn50DCD+9DuU2GyNNsy/qvwza/6u8/Um5mt+Bm6qPzIyYdcNKEbdV49XUkOQ3DvAG/GRxyOQ8C/oVfrZx3/lMYHdwbOolF5TycuH06jTFNiNfpiXx/dQI5efuk85PKmwW79pKeNgjUS+ZwbCbuCv0nhvrJbXgD68Jq1LPpWQSVfDHVcV3Yg70MCZNeDG/dJHgP4lC7+G/ebo3evdbXhcSQz21kZ6fJDOf0AevxZIXG9U0JzMKInnKDFI5sknrzE+oNufo/dQ6QbysYPhmTkIxDM8C28HOxNXhpZAx+OrOUnJTccAOZP8hqf1VY+qs34Lt6EbD+9kk0POFMSPRQZzIRn4EfbT1I50jBDf85q0XXiTxcffijX3Yh7oiR301NzYA3U5zGeYmfv3yOOJ8SMjG10miMdlMEcdyJP5EeB1PkVWsMTxmdOMfFIjKFhncaNhD+JHj+z3wE6M/1vEp/xwxBodUEH8xG/fZF6iVd6V8GC7rWK8dpXS7bC/qdGG9wROSiaPXxI4+Qwx/lUNbbrxAE/0Dv/dmZ/yvSaPHw2+sD1+qh4ZzM+VXoQaOjUD/omTswqhbD1zEPCHx4t5ntbw+T/3eluHTu03d1G70TGgRyYd2b6Hw2VF7ri+VISn5q6MDPSCf3Po8vr3m4GfHTImCX15YJ+CcEQ5/vKXLxM9mbD98UqZMGK8oVazxqGvhJKhJcQLupiH9z5vxdq6zMAP8aUlY5pAUpPoAz7nU/ZUgd7ifCGnaiUj6/VTob8H7zfzeT2tjKDU1WaSUIfrXgIu3PAuj3k0B/n++6xch+jzKu8gI2vEp9/cwmAenzHbIrLyGoauZDXVIyPG64Wi8WFMH8yPytWA95RsEl/JsKPHb+7lN+//JPXvAv+sYJPMM0rYuhSsNye/Mupm+UUnHnlED5SGX2FyEq/yKT64TR5Oib0zHw6vbgd2CteZk5HDd+KHuCPRcx1EKgM8Xr14MQ/xJ53sXnPP34P9/TDP/dwif2Gn10GPLPVMc9cniUEfJlwlcOEyUqeAnb6wI991q2iH+X4zOuzyAtq+QkJTS3WZ0JGVt+ulnI7+SR5/UrLokjGAKoGBoV/ZZJMY2B/ZpD+vGWpgfr1dcQ1di/gHc3rWCpfPh7/TrZnMZzqJw3O4yugL097rId6DfNq7U5/gKVzBzQ6GJD4PKK7TVw3C6I+kxr5D1s/rcPqNCoTCtPjeNG9VKjyLbvuGRZT3EzadJn6yWUCI5zILaTzMCzu1n0Q1cTyh4IP5OQX5nu4+mScnW+VTVSHi3uDzwBr5lddA/IFvyCsZZtenIQd92FqXSVOYpSNz/iSn6Y3VlB6Skb0m65XKi3iP8u7zESXHtheW2I3mJJkBWmEiDa0a5BEsCQL7GrnuwSXNycA+k0nHL7fxj4HPjrYZ7XiIH4sP6QmnMEXQ8ZrifbZqGJ/R38sftQvAjbLGPm8jjb8rqxH2YJ4LukJOPtbnxcCPdDIX8O3N8dQghX0ssTubtEX6wK7blmrVdLsTaswMebrNN5wmG3znVgIrU9izyvvdnfe1OT5VBnJ6hmezoy19PDlZy8am7BPi3W+UGbxHpM9o842yO6GTTdIVviY1e7UUHk+AbzNLDE1N4Yb4kKZoCLLSGoVJhx6WbrMWBzK05B4O7Xg1yfY6MhpNHFDESaI8YlumIJ340dHmcHXow+nD55UY7AV8/WY0tZ99aoWdDqnATrBpjVWkHYN5Qz7kWK2jTQli34Fe0MNdHPo9mR99JpgP+DCPz5TNtEDl07pQn1YKPuonSVapn0jkOZ96c9K+aeHvzr2ZmHfg875ie4fuvP0UzBfxwNe17mKebnyi3/VTEL92MPjIVdgtToI+omTFeDGf4FmkvaDzkr/T8aBLtjZ97kxYYVI+c9nc8Z2h5aUZP5m/Ka2ZMV7Mo+3v1V+/W4xp3Lp2TuI2HFEK8ZQgtAzC38GOuGN5DfWH3CaRK867z5yOwTO3En4O9Lx4mRfonurpKyID9hjgNPFlNRPEczM64HMPR+2OFH4Wcnk8T+uNUKl+gf3MMmT2VYU/C1c83pyd57V5fD9cN/Fpo98R2B16lX9YT+PvmZPdBT5jDeNi1Ex/ZtmK26SqnxJE6Bl19zBJUoPGgCDPKKEP/hDwAbxak3lAOfluYp53g2BD2U+MT89JnTA+g36PKxV/6TrISOLJJgM51L868zKGxUzPymsiN7DPYZ59ZBCPmmmN6fNW92Kefc6deTID0JPHvyg5IDF4QR0B8eE0oBdw3ObB/OkMoMaOIE6oPuvWHGRgZx3ZGBe3YXe3Nywe1zn5u8mCdPvFbWL1+ir9TseAFUdf2bJDy8A5L/QIJPB5hM6quY/DT4WELv2Fu7/1IAnvUXMv5ivCrzWrJDWzs4q0nx7Z8Xj7477fj3Rkq/oTbsgP2OBXXvPuxCtOAn7vUsOCduqnX2t1/Cj+qt0R6DEmrv2qpbrxs3CtIQ6NSVo1eLxTL/SAT3k5T2v+1OSPylVXujOtGTd0eYHuE8iLeLPu9kW8O804XM/pCvNEynJ4nAbw6G25evYzAyhBr0wY3KSToC+niT9G3RCfcRLkLJDQCVXzVnpkcviKkhs8yMeoG84ZF2d3cnSZjPzdE+7jpzVLCGPvCPjVqZnMVyqpB5ZQg9v0jjbvjE72O/Fc6maTwREE8R/4i/JpXNAN4hFI0CPjXjTAId6+SuGvzBduKlfSyf1q/bJxPLExE5Ni1cYJSwQXNvNJqT5prQp9RkoZrO8H9pm26l4SxK7P6x1R1X68liW9HeabFEsabeWTXIsur5mj7nQbpfFEmsX8t93fE/OXN7lGpJkzV4ynlcL1snwd5KtFplsJo4CiQ+ZfkMO8ZiH090jI0GirOUmk6RVIGhfSeZVPy8iOx2/Zp0Ta02fS5xXkw99v9l3ME2Zgvg/P0mW4h4M99Vvdm/nvefWMjoHuEI1LRGKe7h7wqZ4C+/0R8yJe/k57jIQsiB/cO2gN/pouJZkPG0tWifJh5DRUrv7F43EbocdgvvOOzLmjd/t5V62N+ASO3NbNH1awS5eyBn8OPIONnlfgB3aYzxJqg1+Hq5AzcY1f9ffAryVB6MjIhFksJaepbWqrwUfhWvJyXIaG/MGONfDPq6WtvEbdjrTEzelkv+pn9+qv4uTv5DQoU1jbOIcgMN9G9c2IkRs8Hh/E4/MQH/UTMu0tt60oD/Hu7vaPttvm3nuak/hM8l4na4ZJiFdqoF4N1AN+tGq4/gT69PeJ/Qk5017CYH8zxG9NEMQXdp2v3chozhzksdvgM4jgWqjh9b89EQa7aMfjtbgRgXnYVQ+kS65TmOKw3W8C/3L3DZ7zCflhJ7528858Uk4j+B/sWK0rRHsBX/ygxLoQn16TzPvPB+B/EfF7gBM/GWrQkcVPIh/pGEtHeM0D4viAjtME8SefJNrcdbreKV0/Yh4pFqlwd/nod/Qoz9Paw/zYscoYZKjjwU63Y/MePiNb2Dd4tMIwv7Zi+bcFGt08E3CyyeHufXq2oL9O0Sfow1nqjFKIJzPY4Nfr6fe7xtUVZvDXe5P22TkZb3JFGj/khzA/fAb4e4Tzidvsy1tr/sTyQEINUR7iNUnAbSi6CTV9gQT6vdehtAW6kN8egmeEQ5zZPTJn/tG+zDBVf0wt10osKlfgqwBJn1+dGikkHP1KajZ4Xq+x98AZcmH3IE9PWJsPzUQ8m+Pnax6uWxspXFWA2GeMuulog/sDn3enpync4WtFkJhfL//dohq2AZCT4e9bBxdOM7NJyfeQ1dj3h+B1q/vUrni82sIV4dlTqsKVFUFS9tdSYQpX5WNCjqrGFvApj39rM0uYpyv8KfNB/OL9q/S2EB/DM85WcmHaTAd7JcP0atL4D8hroF1aYYbdg/gNfiSTfQxCNjm6ZCx0fvDK6JWQta6wocdhQD6VWBR/1K0fMp8tg6VM2SuCQJ7dSe64citB0zPCJB0Ds6lmYt1n3iGiN4n4MBwe/J8xTwX1RVejIxcW908jNXiC+EReGZna8f3tVe1etwbdHK8QL+wt1Nw/Yf6XLVkt+DXEGUqstSKI28UhF54NeZYEMetG3UFasLXxjC3t+xnzgZ5eTRfwzY1kLOZ15vd1+hXjW29yJmRvYaQG+1ZC0V6d1Y99vu0CKCPQcGmONaXhMj94S/rQGJAZuMuA3X/PAbWZp26ls/oB82gMMjlg22f6O/On8huhh3jNQTifuBUd2KcETtCNc3zeff00tVtW8xnzbIHbqzvAPj2e2i/gs/0CUx4f6DvxCpQwP5JJ0nhGrh/5/PaaerciXsMq6qPf42zVBEd6WwHXm4vXXuHMyVCRrcJVyD9+WEkPzpsh+7vGpRUeVy1XUkNO9lhIlraGlq8Y4Ya0hm58xPlPwBfrYp403kwDV4Zn6Jnw+DXCQWNAJszoLPtMFCCcTpEHF/X/m/nETlLTuzUFHY3BVb+XEV5rhRH2i/i1OZ4jijFI0xikwLyYxz5iPkM8Hk+TSa0mHB6/Ya8wqz6ZFZPVZJ9M9ZP/PhjS5+l0S58p5j+PNnuF+ReFGs4njqeSaO89Bm7z1pywwzyrD82kTYH422HegHM6fQz+Pzvj2IqVFImdAAAAAElFTkSuQmCC",
    rectangleBG: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVcAAAC+CAMAAAC207zUAAAAOVBMVEVHcEw0NDT29vY5OTkyMjJ2dnb39/fg4OAyMjL4+PhDQ0NFRUU/Pz8sLCwvLy+4uLhaWlp6enqYmJj/4XhwAAAACnRSTlMA////xyhD+GZ+xaJXnQAADchJREFUeNrtnY1W5LgOhNsrGBjHCQ3v/7A3v7Zklxynk9yzs6PAAMPpWeCjulyS5ezjwa9fv39//GPX8evj9+9fD+0ypifZYqpG5vRVkn03rV6i2XeJ9Zchuej6ZVhvB2tYbwH7biwuvTaPtSXr4sXLAtadccs4XH6ZXO8TrLnrLQ5rGeuerGU2cI8RGNd7uJq93mOwxuCepGUIjKtxNa6GwLgaV+NqCIyrcTWuhsC4GlfjagiMq3E1robAuBpX42oIjKtxNa6GwLgaV+NqCIyrcTWuhsC4GlfjagiMq3E1robAuBpX42oIjKtxNa6GwLgaV+NqCIyrcTWuhsC4GlfjagiMq3E1robAuBpX42oIjKtxNa6GwLgaV+NqCIyrcTWuhsC4GlfjagiMq3E1robAuBpX42oIjKtxNa6GwLgaV+NqCIyrcTWuX8bgFq4Dff/Y/+PwBq79MAzd95exvez6+PruHsN2GduLmD7deCWubnw1thcwHUlyrjNbZ2xPMl0vyXVBOwze1rIjTH++u0Wlm1oB18kP5oueP5bBdq+vnydtGo1qHRkueaAk6xbhPs0U9p76JJBGH+j7HgrWbXxn4RrcDOnPMzCWw0Z2YFwHIFgXyS6Ppu7b4G7P/NFN8TVIvSK0UbLpofS3w/2oIM2v0V/70mId+8PJjmz/TrgLUnLN12OkWtdr5sg0K9ePC9pfkha+xuUppB+9mSs0ArdKdfkgsaVZtOObMCqXnv9t6U4inVIU0RGlpnVLddg1GWR6pcUPiJZ34T9IdyFKFGWaRHVAr9gJnIgGxT8kec10/3y8H1+LRhfNTO8iTTqqV6hYpzkspZfiCqH7U8X7MUvUj/6WNLNipcNQk79ij02pQGQzhDUEml/Ga/xL9+eod1FoR0gnC9wXqAqubSlWhq6F6fo2zB9NaIMP8wfPfy/fheczhO17h0ijYukE114vvKTH0vq1iJEN/CVenV/ePb9nwB//gqf7iHPU5/S8Wp5bIclCUexJH8jBukqK3ZxAPHkY1sUQIt6RbtcFP17PifD/GfEMc6L59Nt3E0J0rRCfaUCwFDV0HddctdJis1AgFEs51PGPn6/pXbddG+MR8sfVJEeUM8tRmn766t38bkUar8Q22tlFDiu5KmAzxRKPBUivFMC1YY3XSnf8YP74OWEeOc+kZ9b7uD/+mR81/4PpX04cnyPJbn5dYI5PlJno/OUFWAp7gl1/vhckm3Ht1WJWWCzhUJBzpaTY9Sdb2IZI1i8vScR+/bOwXt8+J+jyGj8nH759sCJNv73xdeHp52/DZ3KNgg01wR6uuHKuNcXuZwIGlqBiGVwvf3jPwIKL2UfnuZnAx66/rHitYEOu2F29rpmADnYHANeqYlmPMf4OI9uQOwG32CjZUED1ElQVWcOVM2VkS6zsuVUR7AtG0MAVeizLBMgKKJoXFGwOV5A9CxbodbUADxVLVRtwKca64/2sClgnGlxZIliqPoeWrswIlh8HeMBGltujzuzZplgPFbtaLBTsRhcuXZtez/jAyrXPjGBFm8lV5NiALJYE27BqNRNsxxPCjns267VVsfG71V3guBEArivZHqXYvKgtUkEgxWLXn8VrV+fj2uTPe6zXHLbIBMmwNMXG5ta5dWsD25cjBaXBZqlACDZPWzHtNAj2vBOUbrCBhYKtGAGlFl57KNC5YsHKwgs1CoitA1p9UBWs7/aM4Nkg2I79urK0BcqDiLaStuhIZxtzXQTbl0hRo6DMBYHZQU41BChYXoXtg+2awHa+TbFMr7piD1YHCtd+lmuvjBjJmpay3YPAyG7OhQUbykjgyzLgjGK7phQbWOtYq2ZZQ/8M15lqD+MWSLFZ4RVI7xRwwaIqoaWgal26FIct0ZKouwKpa9dZH1isoIf+mpVd22teHgiwMm15RbKbA4iOwfP1ugvVBz72K+DSRXpBy1IsneC67HuhmhbvIzoMFnUKNp0GWCVsOemkEyDBciPIrYB3YBhctvnsDoStGtdh0KqvQrFUNmMzsiTasVytIXNYn7W4zmTYLmPb6VC38mCnnm0E+6jLFc4YuWyoYGm/5PsH63eIklZIcQAEg+4isLiejZkAwFVC7FapHwG74wNDZQ8h3z+gIm2xnUTKmwXYYjcWLU7wYkHLUqxSzQbc3SaGlc76K2vEOFbKDrAbm0s1Vl+4t8XbIQXb3Wr2eaqeLcuDUM+wq1gv0esKFs8VJItlX0mLBASsoNw/YEqVze6T3dgib9V6sWXH0KX9mOYCoc61bB06fsKjjLGFxa58syay8IJcsV1abc5LttaBKeOWJlhhBE1B63EMq5w2LOOWqA8C2JehwglSkQD6BRfVs6jsYnoltHkg2DqZYffrgza99i0bXlSOw7GObFCdIASZCjrQ4rq2ZVivZ4mQxTrZ2dqTbCPXHvhA3toqOwVxjIQ3OXGnIJQ7Xg1r16uCTVB9WRrAxpbLmwWX+CsYhVHGtvLdg9SPzdcvVh+EMhQUYwY3CXYDS/nubKhbwV4qOM7V6Sl2s5687MrCFkkniFvQUK51sCeb3Kg0wIJ1saJtmyxq5qpMdO/2CWLgEh1k4QShsNirqwN/aOMbTxS4op6toW3n2iuD8mUqwK0tIlB2hWxX75ZQ4GsVbWhtwGQee5UPtCh2q/HS95AGysAoRBrbkj2Dku1dikWLF+kjME4UXlXBHuGKDyGA06EEd7xYjCVZHJQOyy32mg4MmoGReqViHk6SdRnZqmAPcW31WOAEJCcgijnDimC7qzowSLBwVINtfOZoXbaNeBdXV4aCiscWi5ffpouCD1rZ5S/YR/QaWI93D3CjgNWz63CRivYY1wbBph63Q3Er4A0Elich2SvWLm3pYhYLUqzYOpBhq6rX4SRYp8VYqu7RUjZbFCqS9d01iu20tUupuqJiA45bVb0O5wUb78AhfAC1ClhZQFivCtnzqUCNBCGAuMVnS8pAsL6vLV1Hudb2D1SLDeWxDmJlVzb2q6VYf+hp39gyLDrcVNRdQWvA1EZgjnNVOwVDFmN5kg4MbkAbM0KtoTrIee12F5/UB41YWR441uSuO+wLXPvWKXmnHT6gmL7FiZnUZFLWLn9LeaBMabDIBSvZ+up1EVd8pwIifLKj3Jrxu4rtdMXCz/vDHqtNxQZYzIrigC7hqgu2ZFt0CdKGF+WxoKLYrt419KcWr4rDZvPGriwPFCt4DBeBLQaOXfzqsEAgNATDJBugXr2izTbz1QfkZShIdQFoFbhSs7dydWgozmnTRexbZsPctfKg1uluzbc1hy2nuMVpRFxwUToleAVXLWxxJyCw750qBLDznUVYLFgEEKYwLGwNLDtaIpYuwoLdzwQvctUK2la9ZnIt0YZDekUDs/5wyzAojYIgk5bMBITv/PAq1x7PGaJztHk1yzYQ0PnkBLbdYmFrBptDRbA+5I0CvU8Q+wXK3uxjuAZs2j3Y2z5gTkAyxraUs1Cx2AhK/r5T/4uFXGXNXXRfkmL5EPA9XLcQO1QOHwQSra2KzSqCxVaAmrTYMyqH5lCngHbOz2oeeyHX4rSMarF8Io50wVY12wJW+5xmBeA0BzfYkG/H8CZBZrGvc60tXUV14JTtA+SyfrdlWHL1XZtgvToVm1YuD/a6UCTIR2CkXt3LXPvG48kOHPAi1oQheZ+CRpOFyxGSZtOssVBsseEFj3mmiQIwtEUPel2w+J5beYolvVMQoMvWwXaYrG8E673W4OZC9dmO9/ZGPYKUOyw93s5w7Zt7saBTsE0UyGZsu2J9nqnk5zyS8ZK9fDXF+ryxtf3yi6zlxIg8X7/eHm/uDNdeL2irWUtuy5SjW8etAH1SfWBX02vRKIjHfWF/O2Ytrti3x+dwNdcCLWlo0W1L8qNAjU4ABNuhNa44IaPsI6LeFimj3GmsKCr28/F+NVeXOWzZjA38la+7WkUbGp2g/KTyOLzlzQovL9JW7Q4Fjo0YJsW+PyaD7S8XrNxJpIoZgE53lmEbrQD1vlEd4RUn2A4+aTOGFILSgHGySUCTDTzOGkG1aVhYrMOThlROwbT1YHYFW4L1Wm0QZCtWFAfxyFxQDiTL3dnPkeuUtPp7wBbj3GBIHsxtifMqPihbXsecwO8fnBVdbpEKSIYuIk2z2+mVCesk2Ou5Qo9FJ2Vkl7sUbGs9yxck2L7aX7gCPJZMWe0F7gsZJwxXyc5ynRy2vwMsGNwiJRQUBa0vWgUNYNOBDzTmwm7KA84chKxp6PNNrk20yAscW7dWd52ud3cj2Ky7pS1dlTZ32CGL5t31TOAr3cJ08xlZc/ElQDuLyO7Y+L5yHZ3gDq7lZCzpp2UIHU0WN2TZ45rGYoRigRF0Xjklo+iVTZIQHN6kTTHzU/Lz8bgb7KAfl8mdAN3YkA9M7G8kqhvi6HE1wbInSbYdo99bJ3VCGNYRrLtJsei+5/DsXABzhmx4yod9J0iHPwuPlWA7tgsJU6x6/xfU5WaLFrkgsI4e+3ajxdaPy6h3MmOCbUpbWXx6UbD4XiWUzXKDanb62d7eH/l1UrK12xRkIxtqKODTWyG/Zciux8pjX50KNj2ivDUB78WCYYK0csHVKxfrRvbtLrDZhhccjCVwvCu5nJfHksqbRIEDcJoT+JpieTnr8awxoTMdY7zCVGc3+Hx7o+HaTkGhWOywor8hLJaFrba1C0QwHB46pewKhWLlc6nQa3h7+5QO8D9emZGDMPKc7wAAAABJRU5ErkJggg=="
  },

  onLoad: function (options) {
    var param = {};
    if (options.scene) {
      var scene = decodeURIComponent(options.scene).split("&");
      for (var i = 0; i < scene.length; i++) {
        param[scene[i].split("=")[0]] = scene[i].split("=")[1];
      }
    } else {
      param = options;
    }
    console.log(param)
    if (param!=""){
      setTimeout(()=>{
        r.post("wxapp/bindpid", { mid: param.mid }, function (res) {
          console.log(res)
        })
      },1500)
    }
    this.isHideF();
    e.url(options),
      "" == e.getCache("userinfo") && wx.redirectTo({
        url: "/pages/message/auth/index"
      })
  },

  // 是否审核
  isHideF: function () {
    var that = this;
    r.post("/app/ewei_shopv2_api.php?i=1&r=merchshop.switch", {}, function (res) {
      e.globalData.isHide = res.tgs
      that.setData({
        isHide: res.tgs == 1 ? true : false,
      })
    })
  },

  // 获取信息
  getInfo: function () {
    var e = this;
    r.get("/app/ewei_shopv2_api.php?i=1&r=member&comefrom=wxapp&openid=" + getApp().globalData.opId + "&unionid=" + getApp().globalData.unionid, {}, function (r) {
      0 != r.error ? wx.redirectTo({
        url: "/pages/message/auth/index"
      }) :
        e.setData({
          member: r,
          show: !0,
          isinformation: r.isinformation,
        }),
        t.wxParse("wxParseData", "html", r.copyright, e, "5")
    })
  },

  // 获取微信用户信息
  getWXuserInfo: function (event) {
    // console.log(event);
    var that = this;
    if (event.detail.errMsg == "getUserInfo:fail auth deny") {
      return;
    }
    var info = JSON.parse(event.detail.rawData);
    r.post("wxapp.userinfouserinfo", {
      encryptedData: event.detail.encryptedData,
      iv: event.detail.iv,
      sessionKey: getApp().globalData.session_key,
    }, function (res) {
      // console.log(res);
      console.log(res["Another_account"]);
      if (res["Another_account"] != '') {
        wx.showModal({
          title: '提示',
          content: '检测您已是公众号会员，是否进行账号合并？',
          confirmText: '确认合并',
          success() {
            r.post("wxapp.consolidatedAccounts", {
              encryptedData: event.detail.encryptedData,
              iv: event.detail.iv,
              sessionKey: getApp().globalData.session_key,
            }, function (res0) {
              // console.log(res0);
              if (res0.error == 0) {
                r.success("合并成功")
              } else {
                r.alert(res0.message);
              }
            })
          }
        })
      }
      that.getInfo()
    })
    this.setData({
      avatar: info.avatarUrl
    })
  },
  // 跳转到订单页
  toOrder: function(e){
    getApp().globalData.orderStatus = e.currentTarget.dataset.status;
    wx.switchTab({
      url: '/pages/order/index',
    })
  },
  // 扫码
  scan: function () {
    wx.scanCode({
      onlyFromCamera: true,
      success(res) {
        console.log(res)
        wx.navigateTo({
          url: '/' + res.path,
        })
      }
    })
  },
  // 成为推广商
  tobemerchant: function () {
    if (this.data.member.binded == 0) {
      r.toast("请先绑定手机号码", "none")
      setTimeout(() => {
        wx.navigateTo({
          url: '/pages/member/identification/identification',
        })
      }, 1000)
    } else if (this.data.member.is_authentication == 0) {
      r.toast("请先进行身份验证", "none")
      setTimeout(() => {
        wx.navigateTo({
          url: '/pages/member/identification/identification',
        })
      }, 1000)
    } else {
      setTimeout(() => {
        wx.navigateTo({
          url: '/pages/member/tobemerchant/tobemerchant',
        })
      }, 1000)
    }
  },
  // 页面显示
  onShow: function () {
    this.getInfo()
  },
  // 右上角分享
  onShareAppMessage: function () {
    return r.onShareAppMessage()
  },
  //授权获取用户信息
  onGotUserInfo: function (i) {
    var n = this,
      a = e.getCache("userinfo");
    a = i.userInfo;
    if (a && !a.needauth)
      return void (t && "function" == typeof t && t(a));
    if (i.detail.errMsg == 'getUserInfo:ok') {
      wx.login({
        success: function (o) {
          if (!o.code)
            return void r.alert("获取用户登录态失败:" + o.errMsg);
          r.post("wxapp/login", {
            code: o.code
          }, function (o) {
            return o.error ? void r.alert("获取用户登录态失败:" + o.message) : o.isclose && i && "function" == typeof i ? void i(o.closetext, !0) : void r.get("wxapp/auth", {
              data: i.detail.encryptedData,
              iv: i.detail.iv,
              sessionKey: o.session_key
            }, function (e) {
              n.getInfo();
            })
          })
        }
      })
    }
  },
  // 邀请好友二维码
  showQR: function () {
    var that = this;
    r.post("/app/ewei_shopv2_api.php?i=1&r=member.sharecode", {}, function (res) {
      var url = res.img;
      wx.downloadFile({
        url: url,
        success: function (res) {
          //图片保存到本地
          wx.saveImageToPhotosAlbum({
            filePath: res.tempFilePath,
            success: function (data) {
              wx.showToast({
                title: '保存成功',
                icon: 'success',
                duration: 2000
              })

            },
            fail: function (err) {
              console.log(err);
              if (err.errMsg === "saveImageToPhotosAlbum:fail auth deny") {
                console.log("当初用户拒绝，再次发起授权")
                wx.openSetting({
                  success(settingdata) {
                    if (settingdata.authSetting['scope.writePhotosAlbum']) {
                      console.log('获取权限成功，给出再次点击图片保存到相册的提示。')
                    } else {
                      console.log('获取权限失败，给出不给权限就无法正常使用的提示')
                    }
                  }
                })
              }
            },
            complete(res) {
              console.log(res);
            }
          })
        }
      })
      that.setData({
        inviteQR: res.img,
        isShow: true,
      })
    })
  },
  // 关闭海报
  close: function () {
    this.setData({
      isShow: false,
    })
  },
  // 合并账号
  combine: function (e) {
    console.log(e);
    if (e.detail.errMsg == "getUserInfo:fail auth deny") {
      return;
    }
    r.post("wxapp.consolidatedAccounts", {
      encryptedData: e.detail.encryptedData,
      iv: e.detail.iv,
      sessionKey: getApp().globalData.session_key,
    }, function (res0) {
      console.log(res0);
      if (res0.error == 0) {
        r.success("合并成功")
      } else {
        r.alert(res0.message);
      }
    })
  },
  // 签到
  signIn: function () {
    r.post("/app/ewei_shopv2_api.php?i=1&r=member.signin", {}, function (res) {
      if (res.status == 1) {
        wx.showToast({

          title: res.log,

          icon: 'none',

          duration: 1500

        })
      }
    })
  },
  // 商家管理
  merchant: function () {
    var that = this;
    r.post("/app/ewei_shopv2_api.php?i=1&r=merch.registerinfo.regstate", {}, function (res) {
      if (res.error == 0) {
        if (res.status == 1) {
          wx.redirectTo({
            url: '/pages/merchant/index',
          })
        } else {
          wx.navigateTo({
            url: '/pages/merchant/enter/index?status=' + res.status,
          })
        }
      } else {
        r.alert(res.message);
      }
    })
  },
})